#!/bin/bash

# VARIABLES

destination="../../src/main/resources"

# ASCII
l_blue=" \e[94m"
l_green=" \e[92m"
l_red=" \e[91m"
l_yellow=" \e[33m"
reset=" \e[39m"
indent="    "

now=$(date)

log() {
  color=$1
  status=$2
  msg=$3
  echo -e "${color}${now} [ ${status} ]${indent}${reset}${msg}"
}
debug() {
  log "$l_green" "DEBUG" "${1}"
}

warn() {
  log "${l_yellow}" "WARN " "${1}"
}

error() {
  log "${l_red}" "ERROR" "${1}"
}

notice() {
  log "${l_blue}" "NOTE " "${1}"
}

clean_up() {
  file=$1
  if test -f "$file"; then
    warn "Remove old $file"
    rm "$file"
  fi
}

debug "Start script"

NAME=''
PASS=''
if [ -z "$1" ]; then
  NAME="keyStore"
  warn "Default file name"
else
  NAME=$1 # name of a file that will be created
  debug "Filename: $NAME"
fi
if [ -z "$2" ]; then
  PASS="secret"
  warn "Default password"
else
  PASS=$2 # password for a file that will be created
  debug "Password: $PASS"
fi

file="${NAME}.jks"

FILES="$(pwd)"
if [[ $FILES != *key* ]]; then
  warn "Enter assets"
  cd ./assets/key || exit 0
fi

read_file() {
  name=$1
  value=$(<"$name")
  echo "${value}"
}

file_contains() {
  target=$1
  substring=$2
  content="$(read_file "$target")"
  echo "$content" | grep -q "$substring"
  echo $?
}

clear

clean_up "$file"
clean_up "${destination}${file}"

gen() {
  name=$1
  pass=$2
  debug "Generate ${name^}"
  keytool -genkeypair \
    -alias "${name}" \
    -keyalg RSA -keypass "$pass" \
    -keystore "${name}.jks" \
    -storepass "$pass" \
    -dname "CN=, OU=, O=, L=, S=, C=" \
    -noprompt
}

certificate="key: "
gen "$NAME" "$PASS"

readKey() {
  name=$1
  pass=$2
  echo
  out="$(
    echo "$pass" |
      keytool -list -rfc --keystore "${name}.jks" -dname "yes" |
      openssl x509 -inform pem -pubkey
  )"
  debug "$out"
  start_line="-----BEGIN"
  if [ -z "${out}" ]; then
    error "${name} file is empty exit"
    exit 1
  fi
  for line in $out; do
    if test "${line}" == "CERTIFICATE-----"; then
      break
    elif [[ ${certificate} =~ "-----END PUBLIC KEY-----" ]]; then
      notice "public certificate received"
      break
    else
      if test "${line}" == "$start_line"; then
        certificate+="${line} "
      elif test "${line}" == "-----END"; then
        certificate+="${line} "
      elif test "${line}" == "KEY-----"; then
        certificate+=" ${line}"
      else
        certificate+="${line}"
      fi
    fi
  done
}

readKey "$NAME" "$PASS"

cd ../..
cp "assets/key/${NAME}.jks" src/main/resources
clean_up "${NAME}.jks"

if test -f "src/main/resources/${NAME}.jks"; then
  echo
  git rm --cached -r "src/main/resources/${NAME}.jks"
fi

result="$(file_contains .gitignore "src/main/resources/${name}.jks")"
is_key="$(file_contains "resource/src/main/resources/application.yml" "key: ")"

if test "${result}" == "1"; then
  warn ".gitignore already contains target file"
else
  echo "src/main/resources/${NAME}.jks" >>.gitignore
  debug "${NAME}.jks added to .gitignore"
fi

if test "${is_key}" == "1"; then
  echo "$certificate" >>resource/src/main/resources/application.yml
  debug "Public certificate added to application.yml"
else
  sed -i '/^key:.*/ d' resource/src/main/resources/application.yml
  echo "$certificate" >>resource/src/main/resources/application.yml
  debug "Public certificate added to application.yml"
fi

notice "All done"
exit 0
