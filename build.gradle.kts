@file:Suppress("SpellCheckingInspection")

import java.io.FileInputStream
import java.util.*

buildscript {
    val springBootVersion = "2.4.1"
    val kotlinVersion = "1.4.21"
    configurations {
        classpath {
            exclude(group = "ch.qos.logback", module = "logback-classic")
        }
    }
    repositories {
        google()
        jcenter()
        mavenCentral()
        maven { url = uri("https://maven.springframework.org/milestone/") }
    }
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${kotlinVersion}")
        classpath("org.jetbrains.kotlin:kotlin-allopen:${kotlinVersion}")
        classpath("org.jetbrains.kotlin:kotlin-noarg:${kotlinVersion}")
    }
}

plugins {
    id("org.springframework.boot") version "2.4.1"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    kotlin("jvm") version "1.4.21"
    kotlin("plugin.spring") version "1.4.21"
}

group = "PL"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
    maven { url = uri("https://repo.spring.io/milestone") }
    maven { url = uri("https://maven.springframework.org/milestone/") }
}

val data = FileInputStream("gradle.properties")
val props = Properties()
props.load(data)

dependencies {
    implementation(kotlin("reflect"))
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.cloud:spring-cloud-starter-oauth2:${properties["springCloudVersion"]}")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("io.projectreactor:reactor-test")
    testImplementation("org.springframework.security:spring-security-test")

    runtimeOnly("org.postgresql:postgresql")

    implementation("com.fasterxml.jackson.core:jackson-databind:${properties["jacksonVersion"]}")
    implementation("com.fasterxml.jackson.core:jackson-annotations:${properties["jacksonVersion"]}")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:${properties["jacksonVersion"]}")
    implementation("org.hibernate:hibernate-core:5.4.27.Final")

}

tasks.register<Exec>("createKey") {
    if (System.getProperty("os.name").toLowerCase().contains("windows")) {
        commandLine("echo 'Not supported'")
    }
    commandLine(
        /* exec file */ "./assets/key/genkey.sh",
        /* generate file name */ "store",
        /* generate file password */ "secret"
    )
}

configurations.all {
    resolutionStrategy.eachDependency {
        if (requested.name == "log4j") {
            useTarget("log4j:log4j:1.2.+")
        }
        if (requested.name == "org.springframework.boot") {
            useTarget("spring-boot-starter:2.4.1")
        }
        if (requested.name == "org.springframework.cloud") {
            useTarget("spring-cloud-dependencies:${property("springCloudVersion")}")
        }
    }
}

dependencyManagement {
    imports { mavenBom("org.springframework.boot:spring-boot-starter:2.4.1") }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

