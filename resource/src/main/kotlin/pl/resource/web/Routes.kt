package pl.resource.web

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.function.router
import pl.resource.web.handler.RouterHandler

@Configuration
class Routes(private val handler: RouterHandler) {
    @Bean(name = ["serverRoutes"])
    fun routes() = router {
        GET("/") { ok().body("index.html")}
        GET("/hello", handler::hello)
    }
}