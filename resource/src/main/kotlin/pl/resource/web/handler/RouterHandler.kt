package pl.resource.web.handler

import org.springframework.stereotype.Service
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse.ok

@Service
class RouterHandler {

    fun hello(request: ServerRequest) = ok().body("hello")
}