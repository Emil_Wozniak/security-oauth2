[[_TOC_]]

# Requirements

- **Kotlin** 1.4.21 or current
- **PostgresQL** 12

## Configurations

### Environment variables:

You must add below *Environment variables* to your system of IDE for all **Services**:

| name       | value  |
|------------|--------|
| signingKey | secret |

Read an [application.yml](src/main/resources/application.yml) to make sure how to configure your database and then fire
up [Schema](src/main/resources/schema.sql).

## Structure of **Services**

- [Authorization Server](src/main/kotlin/pl/securityoauth2)
- [Resource Server](resource/src/main/kotlin/pl/resource)

# Plant UML sequence diagrams

[User grant type](assets/uml/user-grant-type.puml)

[Authorization code](assets/uml/authorization-code.puml)

[Client credentials](assets/uml/client-credentials.puml)

# Lesson 11:

- Step 1

```http request
http://localhost:8080/oauth/authorize?response_type=code&client_id=redirected&scope=read
```

- Step 2 Login and get `code` from response


- Step 3 Insert code from step 2 into code param

```http request
localhost:8080/oauth/token?grant_type=authorization_code&scope=read&code={code}
```

# Lesson 14 - Using Opaque Tokens - Introspection

**Authorization Server** is called by **Resource Server** for access token.

# Lesson 15 - Using Opaque Tokens - Blackboard

It's a kind of anti-pattern, both sides **Authorization Server** and **Resource Server**
calls the same database.

## Example:

Authorization Server Authorization -> Basic Auth:

- **username**: Client1
- **password**: secret

```http request
http://localhost:8080/oauth/token?grant_type=password&username=emil&password=1234&scope=ROLE_USER
```

Resource Server Headers: **Authorization**: Bearer $access_token

```http request 
http://localhost:9090/hello
```

# Lesson 17 - Using symmetric keys with JWT

See [Schema](src/main/resources/schema.sql) and in query `INSERT INTO oauth_client_details`
you can find create a query for 2 clients.

In this authorization type we need a client with `grant_type`: **authorization_code**, which in my example is **
client2**

Below request, you should execute in a web browser. **client2** is a client who can obtain and use authorize code.

## Request code

```http request
http://localhost:8080/oauth/authorize?response_type=code&client_id=client2&scope=ROLE_USER
```

Now you will be prompt to log in and accept authorization, then web browser redirect you to a client **
redirectUri** `http://localhost:9090/?code=$CODE`

## Use Authorization code

In this step you need to use [**Postman**](https://www.postman.com/), go to
**Authorization**: Basic Auth and insert client credentials as below:

- **username:** client2
- **password:** secret

```http request
http://localhost:8080/oauth/token?grant_type=authorization_code&scope=ROLE_USER&code=$CODE
```

## Lesson 18 - Using asymmetric key pairs with JWT

Keys (*signingKey*) should not be shared between [rs](#rs) and [as](#as) because they are probably belonged to different
organization

```mermaid
graph LR
    A[User] -- web browser --> B((Client))
    B((Client)) <-- Token --> C(Organization A) -- Authorization server --> E{{KEY private}}
    B((Client)) -- Token --> D[(Organization B)] -- Resource server --> F{{KEY public}}
```

### Generate key (optional)

Execute [genkey](assets/key/genkey.sh), by `./gradlew createKey`, new keyStore will be generated
for you don't need to copy keyStore or *public key* value. All will be set up.

How it works:
- keytool will generate 

## Symbols

### rs

Resource server

### as

Authorization server