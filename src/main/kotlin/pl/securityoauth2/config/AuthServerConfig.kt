package pl.securityoauth2.config

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.core.env.get
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpMethod.GET
import org.springframework.http.HttpMethod.POST
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer
import org.springframework.security.oauth2.provider.ClientDetailsService
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory
import pl.securityoauth2.config.constant.KEY
import pl.securityoauth2.config.constant.KEY_STORE
import pl.securityoauth2.config.constant.STORE_NAME
import pl.securityoauth2.config.constant.TokenAccess.IS_AUTHENTICATED
import pl.securityoauth2.security.JpaClientDetailsService

@Configuration
@EnableAuthorizationServer
class AuthServerConfig(
    private val env: Environment,
    private val authenticationManager: AuthenticationManager
) : AuthorizationServerConfigurerAdapter() {

    val log: Logger = LoggerFactory.getLogger(this::class.java.simpleName)

    @Bean
    fun tokenStore(): TokenStore = JwtTokenStore(converter())

    @Bean
    fun jpaClientDetailsService(): ClientDetailsService = JpaClientDetailsService()

    /**
     * Previously key was obtain by setSigningKey(env[KEY]).
     */
    @Bean
    fun converter(): JwtAccessTokenConverter = JwtAccessTokenConverter().run {
        with(createStore(KEY, KEY_STORE)) {
            setKeyPair(this.getKeyPair(STORE_NAME))
        }
        this
    }

    override fun configure(clients: ClientDetailsServiceConfigurer) {
        clients.withClientDetails(jpaClientDetailsService())
    }

    /**
     * Must contain:
     * - <b>tokenStore</b>
     * - <b>accessTokenConverter</b>
     */
    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer) {
        endpoints
            .allowedTokenEndpointRequestMethods(GET, POST)
            .authenticationManager(authenticationManager)
            .tokenStore(tokenStore())
            .accessTokenConverter(converter())

    }

    override fun configure(security: AuthorizationServerSecurityConfigurer) {
        security.tokenKeyAccess(IS_AUTHENTICATED.value)
    }

    /**
     * @param key is the name of environment variable that is holding password for
     * keystore.
     * @param keyStore is the name of the key store in the classpath.
     * @see KEY is the name of environment variable that is holding password for
     * keystore.
     * @see KEY_STORE is the name of the key store in the classpath.
     */
    @Suppress("SameParameterValue")
    private fun createStore(key: String, keyStore: String): KeyStoreKeyFactory {
        val password = env[key]?.toCharArray() ?: CharArray(0)
        val resource = ClassPathResource(keyStore)
        log.info("$password $resource")
        return KeyStoreKeyFactory(resource, password)
    }
}


