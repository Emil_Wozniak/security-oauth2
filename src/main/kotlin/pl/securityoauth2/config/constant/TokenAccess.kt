package pl.securityoauth2.config.constant

enum class TokenAccess(val value: String) {
    PERMIT_ALL("permitAll()"),
    IS_AUTHENTICATED("isAuthenticated()");

}