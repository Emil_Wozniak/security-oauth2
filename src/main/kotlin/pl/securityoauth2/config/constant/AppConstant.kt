package pl.securityoauth2.config.constant

const val EMOJI = "\uD83D\uDE26"
const val name = "client"
const val KEY = "signingKey"
const val KEY_STORE = "keyStore.jks"
const val STORE_NAME = "keyStore"