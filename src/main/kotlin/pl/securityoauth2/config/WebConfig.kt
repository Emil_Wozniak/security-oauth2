package pl.securityoauth2.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.NoOpPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import pl.securityoauth2.repo.UserRepo
import pl.securityoauth2.service.JpaUserDetailsService

@Configuration
class WebConfig(private val users: UserRepo) : WebSecurityConfigurerAdapter() {

    /**
     * Should not be call userDetailsService
     */
    @Bean
    fun detailsService(): UserDetailsService = JpaUserDetailsService(users)
    override fun configure(http: HttpSecurity) {
        http
            .authorizeRequests()
            .antMatchers("/oauth/**")
            .permitAll().and()
            .authorizeRequests()
            .anyRequest().hasRole("USER")
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder = NoOpPasswordEncoder.getInstance()

    @Bean
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager? =
        super.authenticationManagerBean()
}
