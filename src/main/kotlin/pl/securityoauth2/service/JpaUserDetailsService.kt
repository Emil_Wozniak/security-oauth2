package pl.securityoauth2.service

import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import pl.securityoauth2.config.constant.EMOJI
import pl.securityoauth2.repo.UserRepo
import pl.securityoauth2.security.model.SecureUser

class JpaUserDetailsService(private val repo: UserRepo) : UserDetailsService {
    override fun loadUserByUsername(username: String): UserDetails? =
        repo.findByUsername(username)
            .map { if (it != null) SecureUser(it) else null }
            .orElseThrow { UsernameNotFoundException("Unknown user $EMOJI") }
}