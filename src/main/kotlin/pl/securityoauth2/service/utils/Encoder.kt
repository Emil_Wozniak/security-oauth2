package pl.securityoauth2.service.utils

import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

//@Component
class Encoder(private val encoder: PasswordEncoder) {
    infix fun encode(password: String): String =
        encoder.encode(password)
}