package pl.securityoauth2.service.utils

/**
 * Function checks type and state of the consume data, and if it fits requirements
 * it perform action.
 *
 * Example:
 *     `.mapNotNull { data: String -> transform(data) { it.split(":") } }`
 * @param D insert data type
 * @param predicate custom condition for data
 * @param R result data type
 */
inline fun <reified D, R> transform(
    data: D,
    predicate: (D) -> Boolean = { if (it is String) it.isEmpty() else false },
    action: (value: D) -> R?
): R? =
    when (data) {
        predicate(data) -> action(data)
        data is String && data.isNotBlank() -> action(data)
        data is List<*> && data.isNotEmpty() -> action(data)
        else -> null
    }