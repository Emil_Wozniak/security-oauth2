package pl.securityoauth2.repo

import org.springframework.data.jpa.repository.JpaRepository
import pl.securityoauth2.model.security.User
import java.util.*

interface UserRepo: JpaRepository<User, Long> {
    fun findByUsername(username: String): Optional<User?>
}