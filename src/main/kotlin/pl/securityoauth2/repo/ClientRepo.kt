package pl.securityoauth2.repo

import org.springframework.data.jpa.repository.JpaRepository
import pl.securityoauth2.model.security.Client
import java.util.*

interface ClientRepo: JpaRepository<Client, Long> {
    fun findClientByClientId(clientId: String?): Optional<Client?>
}