package pl.securityoauth2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
class SecurityOauth2Application

fun main(args: Array<String>) {
    runApplication<SecurityOauth2Application>(*args)
}
