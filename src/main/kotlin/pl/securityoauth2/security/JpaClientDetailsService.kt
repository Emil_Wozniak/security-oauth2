package pl.securityoauth2.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.oauth2.provider.ClientDetails
import org.springframework.security.oauth2.provider.ClientDetailsService
import org.springframework.security.oauth2.provider.ClientRegistrationException
import pl.securityoauth2.config.constant.EMOJI
import pl.securityoauth2.repo.ClientRepo
import pl.securityoauth2.security.model.SecurityClient

class JpaClientDetailsService : ClientDetailsService {
    @Autowired
    private lateinit var clients: ClientRepo

    override fun loadClientByClientId(clientId: String): ClientDetails? = clients
        .findClientByClientId(clientId)
        .map { it?.run { SecurityClient(this) } }
        .orElseThrow { ClientRegistrationException("Client not fount $EMOJI") }
}