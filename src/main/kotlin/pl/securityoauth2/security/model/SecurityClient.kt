package pl.securityoauth2.security.model

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.oauth2.provider.ClientDetails
import pl.securityoauth2.model.enumaration.GrantType.Companion.isValidType
import pl.securityoauth2.model.security.Authority
import pl.securityoauth2.model.security.Client
import pl.securityoauth2.service.utils.transform
import kotlin.reflect.full.memberProperties

data class SecurityClient(val client: Client) : ClientDetails {

    override fun getClientId(): String = client.clientId

    override fun isSecretRequired(): Boolean = true

    override fun getClientSecret(): String? = client.secret

    override fun isScoped(): Boolean = true

    override fun getResourceIds(): Set<String>? = null

    override fun getScope(): Set<String?> = valuesOf("scope").toSet()

    override fun getAuthorities(): Collection<GrantedAuthority> = scopes()

    override fun getAuthorizedGrantTypes(): Set<String?> = valuesOf("grantType").toSet()

    override fun getRegisteredRedirectUri(): Set<String?> = valuesOf("redirectUri").toSet()

    override fun getAccessTokenValiditySeconds(): Int = client.accessTokenValidity

    override fun getRefreshTokenValiditySeconds(): Int = client.refreshTokenValidity

    override fun isAutoApprove(scope: String?): Boolean = false

    override fun getAdditionalInformation(): Map<String, Any> = mapOfAdditional()
}

@Suppress("UNCHECKED_CAST")
private fun <String> SecurityClient.valuesOf(field: String): Collection<kotlin.String> =
    this.client::class
        .memberProperties
        .filter { it.name == field }
        .map { it.getter.call(this.client) as kotlin.String }
        .map { it.split(",") }[0]

private fun SecurityClient.mapOfAdditional(): Map<String, Any> = this.valuesOf("additionalInformation")
    .asSequence()
    .mapNotNull { data -> transform(data) { it.split(":") } }
    .mapNotNull { data -> transform(data) { it[0] to it[1] as Any } }
    .toMap()

private fun SecurityClient.scopes() = this.valuesOf("scope")
    .filter { isValidType(it) }
    .mapNotNull { data -> transform(data) { Authority(it) } }
    .toList()