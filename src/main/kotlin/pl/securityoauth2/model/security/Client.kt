package pl.securityoauth2.model.security

import javax.persistence.*
import javax.persistence.GenerationType.IDENTITY

@Entity
@Table(name = "oauth_client_details")
data class Client(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    val id: Long? = null,

    @Column(name = "client_id")
    var clientId: String = "",

    @Column(name = "secret")
    val secret: String? = null,

    @Column(name = "scope")
    var scope: String? = null,

    @Column(name = "grant_type")
    var grantType: String = "ROLE_USER",

    @Column(name = "redirect_uri") var redirectUri: String = "",

    @Column(name = "access_token_validity")
    var accessTokenValidity: Int = 43199,

    @Column(name = "refresh_token_validity")
    var refreshTokenValidity: Int = 43199,

    @Column(name = "additional_information", length = 4096)
    var additionalInformation: String? = null,

    @Column(name = "auto_approve")
    var autoApproveScopes: String? = null

)
