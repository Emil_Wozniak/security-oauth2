package pl.securityoauth2.model.security

import javax.persistence.*
import javax.persistence.GenerationType.*

@Entity
@Table(name = "oauth_user")
data class User(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    var id: Long? = null,
    var username: String = "",
    var password: String = "",
    var authorities: String = "ROLE_USER"

)
