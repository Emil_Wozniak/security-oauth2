package pl.securityoauth2.model.security

import org.springframework.security.core.GrantedAuthority

data class Authority(
    var name: String = ""
) : GrantedAuthority {
    override fun getAuthority(): String = name
}