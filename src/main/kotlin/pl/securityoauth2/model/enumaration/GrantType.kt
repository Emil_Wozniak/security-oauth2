@file:Suppress("unused")

package pl.securityoauth2.model.enumaration

import kotlin.DeprecationLevel.ERROR
import kotlin.DeprecationLevel.WARNING

/**
 * OAuth Grant types:
 */
const val REPLACE = "AUTHORIZATION_CODE, CLIENT_CREDENTIALS, REFRESH_TOKEN"
const val MSG = "Not supported, can cause serious vulnerability."

enum class GrantType {

    AUTHORIZATION_CODE,

    CLIENT_CREDENTIALS,

    REFRESH_TOKEN,

    @Deprecated(message = MSG, level = WARNING, replaceWith = ReplaceWith(REPLACE))
    PASSWORD,

    @Deprecated(message = MSG, level = ERROR, replaceWith = ReplaceWith(REPLACE))
    IMPLICIT;

    companion object {
        private val types = values().map { it.name }
        fun isValidType(type: String): Boolean =
            types.contains(type.toUpperCase())
    }
}