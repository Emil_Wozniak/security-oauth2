DROP TABLE IF EXISTS oauth_client_details;
DROP TABLE IF EXISTS oauth_user;

CREATE TABLE IF NOT EXISTS oauth_user
(
    id          BIGSERIAL PRIMARY KEY NOT NULL,
    username    varchar(50)           NOT NULL,
    password    varchar(255)          NOT NULL,
    authorities varchar(500)          NOT NULL
);

CREATE TABLE IF NOT EXISTS oauth_client_details
(
    id                     BIGSERIAL PRIMARY KEY NOT NULL,
    client_id              VARCHAR(256),
    secret                 VARCHAR(256),
    scope                  VARCHAR(256),
    grant_type             VARCHAR(256),
    redirect_uri           VARCHAR(256),
    access_token_validity  INTEGER,
    refresh_token_validity INTEGER,
    additional_information VARCHAR(4096),
    auto_approve           VARCHAR(256)
);

INSERT INTO oauth_user
VALUES (1, 'emil', '1234', 'ROLE_USER');

INSERT INTO oauth_client_details
(id, client_id, secret, scope, grant_type, redirect_uri, access_token_validity,
 refresh_token_validity, additional_information, auto_approve)
VALUES (1, 'client1', 'secret', 'ROLE_USER', 'password', 'http://localhost:9090', 36000, 36000, null, true),
       (2, 'client2', 'secret', 'ROLE_USER', 'authorization_code', 'http://localhost:9090', 36000, 36000, null, true);
